package main

import (
	"errors"

	trans "github.com/hekmon/transmissionrpc"
	"github.com/sirupsen/logrus"
)

var (
	ErrPeerPortNotOpen = errors.New("peer Port is not open")
)

type TorrentClient interface {
	UpdateForwardedPort(port int) error
	VerifyPortOpen() error
}

type TransmissionClient struct {
	client *trans.Client
	logger logrus.FieldLogger
}

func NewTransmissionClient(username, password, host string, port int) (*TransmissionClient, error) {
	transClient, err := trans.New(host, username, password, &trans.AdvancedConfig{
		Port:      uint16(port),
		UserAgent: "transmission-buddy",
	})
	if err != nil {
		return nil, err
	}
	return &TransmissionClient{
		client: transClient,
		logger: logrus.WithField("component", "TransmissionClient"),
	}, nil
}

func (t *TransmissionClient) UpdateForwardedPort(port int) error {
	t.logger.WithField("port", port).Debug("Updating peer port")
	peerPort := int64(port)
	err := t.client.SessionArgumentsSet(&trans.SessionArguments{
		PeerPort: &peerPort,
	})
	if err != nil {
		t.logger.WithField("port", port).WithError(err).Error("Failed to set peer port")
	}

	return err
}

func (t *TransmissionClient) VerifyPortOpen() error {
	portOpen, err := t.client.PortTest()
	if err != nil {
		t.logger.WithError(err).Error("Failed to communicate with transmission")
		return errors.New("failed to check for open port")
	}
	if !portOpen {
		return ErrPeerPortNotOpen
	}
	return nil
}
