package main

import (
	"context"
	"fmt"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/sirupsen/logrus"
)

type VpnPortSource interface {
	ForwardedPort() int
	ForwardedPortChanged() chan int
}

type PlainFilePortWatcher struct {
	portDir      string
	portFileName string
	watcher      *fsnotify.Watcher
	logger       logrus.FieldLogger
	portChan     chan int

	port int
}

func NewPlainFilePortWatcher(ctx context.Context, portFilePath string) (*PlainFilePortWatcher, error) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}
	portDir := path.Dir(portFilePath)
	portFileName := path.Base(portFilePath)
	pfWatcher := &PlainFilePortWatcher{
		portDir:      portDir,
		portFileName: portFileName,
		watcher:      watcher,
		logger:       logrus.WithField("component", "PlainFilePortWatcher"),
		portChan:     make(chan int, 10),
	}

	if port, err := pfWatcher.readPortFile(portFilePath); err == nil {
		pfWatcher.portChan <- port
	} else {
		pfWatcher.logger.WithError(err).Error("Failed to read port file on startup")
	}

	if buf, err := os.ReadFile(portFilePath); err != nil {
		// Try to read an existing port file
		portString := strings.TrimSpace(string(buf))
		port, err := strconv.Atoi(portString)
		if err != nil {
			pfWatcher.logger.WithField("port", port).Debug("Read port file on startup")
			pfWatcher.port = port
			pfWatcher.portChan <- port
		}
	}
	go pfWatcher.watch(ctx)
	return pfWatcher, nil
}

func (w *PlainFilePortWatcher) readPortFile(portPath string) (int, error) {
	if buf, err := os.ReadFile(portPath); err != nil {
		return -1, fmt.Errorf("failed to read port file: %w", err)
	} else {
		portString := strings.TrimSpace(string(buf))
		port, err := strconv.Atoi(portString)
		if err != nil {
			return -1, fmt.Errorf("invalid content of port file, can't be parsed to port number: %w", err)
		}
		if port < 1 {
			return -1, fmt.Errorf("invalid port number %d", port)
		}
		return port, nil
	}
}

func (w *PlainFilePortWatcher) watch(ctx context.Context) {
	ticker := time.NewTicker(time.Second * 2)
	portPath := path.Join(w.portDir, w.portFileName)
	for {
		select {
		case <-ctx.Done():
			w.logger.Info("Closing")
			w.watcher.Close()
			ticker.Stop()
			close(w.portChan)
			return
		case <-ticker.C:
			w.logger.Debug("Polling port file")
			port, err := w.readPortFile(portPath)
			if err != nil {
				w.logger.WithError(err).Error("failed to read port from file")
				continue
			}
			if w.port != port {
				w.logger.WithField("port", port).Debug("Read new port from port file")
				w.port = port
				w.portChan <- port
			}
		case evt := <-w.watcher.Events:
			if evt.Op != fsnotify.Remove && path.Base(evt.Name) == w.portFileName {
				w.logger.Debug("Received fs event on port file")
				port, err := w.readPortFile(portPath)
				if err != nil {
					w.logger.WithError(err).Error("Failed to parse port from port file")
					continue
				}
				w.logger.WithField("port", port).Debug("Read new port from port file")
				w.port = port
				w.portChan <- port
			} else {
				w.logger.WithFields(logrus.Fields{
					"fileName": evt.Name,
					"event":    evt.Op.String(),
				}).Debug("Received ignored fs event")
			}
		}
	}
}

func (w *PlainFilePortWatcher) ForwardedPort() int {
	return w.port
}

func (w *PlainFilePortWatcher) ForwardedPortChanged() chan int {
	return w.portChan
}
