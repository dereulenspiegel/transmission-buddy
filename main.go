package main

import (
	"context"
	"flag"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var (
	version = "undefined"
	builtBy = "unknown"
	date    = "unknown"
	commit  = "unknown"
)

func init() {
	setDefaults()
}

func setDefaults() {
	viper.SetDefault("transmission.host", "127.0.0.1")
	viper.SetDefault("transmission.port", 9091)
	//	viper.SetDefault("transmission.rpcuri", "/transmission/rpc")
	viper.SetDefault("log.level", logrus.InfoLevel.String())
	viper.SetDefault("port.path", "/pia-shared/port.dat")
	viper.SetDefault("health.addr", ":8080")
}

func readConfig() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")

	setDefaults()
	flag.Parse()

	viper.SetEnvPrefix("BUDDY")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		logrus.WithError(err).Error("Failed to read configuration")
	}
	logLevel, err := logrus.ParseLevel(viper.GetString("log.level"))
	if err != nil {
		logrus.SetLevel(logrus.DebugLevel)
		logrus.WithError(err).Error("Failed to parse log level from config")
	} else {
		logrus.SetLevel(logLevel)
	}
}

func main() {
	readConfig()
	logger := logrus.WithField("version", version)
	logger.WithFields(logrus.Fields{
		"buildDate": date,
		"commit":    commit,
		"buildBy":   builtBy,
	}).Info("Starting transmission-buddy")

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	notifyCtx, notifyCancel := context.WithCancel(ctx)
	defer notifyCancel()

	portFileWatcher, err := NewPlainFilePortWatcher(notifyCtx, viper.GetString("port.path"))
	if err != nil {
		logger.WithError(err).Fatal("Failed to contact watch vpn port")
	}

	torrentClient, err := NewTransmissionClient(viper.GetString("transmission.username"),
		viper.GetString("transmission.password"), viper.GetString("transmission.host"), viper.GetInt("transmission.port"))
	if err != nil {
		logger.WithError(err).Fatal("Failed to contact torrent client")
	}
	torrentHealth := NewTorrentHealthCheck(torrentClient)

	go func() {
		for port := range portFileWatcher.ForwardedPortChanged() {
			logger.WithField("port", port).Debug("Received new peer port for torrent client, updating...")
			if err := torrentClient.UpdateForwardedPort(port); err != nil {
				logger.WithError(err).Error("Failed to update peer port on torrent client")
			}
			logger.WithField("port", port).Debug("Updated peer port")
		}
	}()

	go func() {
		setupHealthCheck(logger.WithField("component", "healthcheck"), torrentHealth)
	}()

	select {
	case <-sigs:
		cancel()
		logger.Info("Shutting down")
		os.Exit(0)
	}
}

func setupHealthCheck(logger logrus.FieldLogger, healtChecks ...HealthCheck) {
	logger = logger.WithFields(logrus.Fields{
		"addr": viper.GetString("health.addr"),
	})
	logger.Info("Setting up healthcheck")
	handlr := http.NewServeMux()
	healthService := NewHealthCheckService(healtChecks...)

	handlr.Handle("/health", healthService)
	httpSrv := &http.Server{
		Addr:    viper.GetString("health.addr"),
		Handler: handlr,
	}
	logger.Info("Listening for healthchecks")
	if err := httpSrv.ListenAndServe(); err != http.ErrServerClosed {
		logger.WithError(err).Fatal("Failed to run http health status server")
	}
	logger.Info("Healtcheck service stopped")
}
