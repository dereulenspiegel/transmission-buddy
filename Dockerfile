FROM scratch

COPY transmission-buddy /transmission-buddy
ENTRYPOINT ["/transmission-buddy"]
