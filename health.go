package main

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

type HealthError struct {
	Err  error
	Name string
}

func (h HealthError) Error() string {
	return h.Err.Error()
}

func (h HealthError) Unwrap() error {
	return h.Err
}

type HealthCheck interface {
	IsHealthy() error
}

type TorrentHealthCheck struct {
	torrentClient TorrentClient
}

func NewTorrentHealthCheck(torrentClient TorrentClient) *TorrentHealthCheck {
	return &TorrentHealthCheck{
		torrentClient: torrentClient,
	}
}
func (t *TorrentHealthCheck) IsHealthy() error {
	err := t.torrentClient.VerifyPortOpen()
	if err != nil {
		// Retry as transmission likes to only report correct status on the second try
		time.Sleep(time.Millisecond * 750)
		err := t.torrentClient.VerifyPortOpen()
		if err != nil {
			return HealthError{
				Err:  err,
				Name: "torrent",
			}
		}
	}
	return nil
}

type HealthCheckService struct {
	healthChecks []HealthCheck
	logger       logrus.FieldLogger
}

func NewHealthCheckService(healthChecks ...HealthCheck) *HealthCheckService {
	h := &HealthCheckService{
		healthChecks: make([]HealthCheck, 0, 10),
		logger:       logrus.WithField("component", "HealthCheckService"),
	}
	h.healthChecks = append(h.healthChecks, healthChecks...)
	return h
}

func (h *HealthCheckService) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.logger.Debug("Health check called")
	healthErrors := make([]error, 0, 2)
	for _, check := range h.healthChecks {
		if check == nil {
			h.logger.Warn("Configured health check was nil")
			continue
		}
		if err := check.IsHealthy(); err != nil {
			healthErrors = append(healthErrors, err)
		}
	}
	if len(healthErrors) == 0 {
		h.logger.Debug("Health check ok")
		w.WriteHeader(http.StatusOK)
		return
	}

	w.WriteHeader(http.StatusServiceUnavailable)
	for _, err := range healthErrors {
		healthErr := HealthError{}
		if errors.As(err, &healthErr) {
			h.logger.WithField("healthCheckName", healthErr.Name).WithError(healthErr.Err).Warn("Health Check failed")
			w.Write([]byte(fmt.Sprintf("[%s] %s\n", healthErr.Name, healthErr.Err)))
		} else {
			h.logger.WithError(err).Warn("Health Check failed")
			w.Write([]byte(fmt.Sprintf("%s\n", err)))
		}
	}
}
