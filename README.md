# transmission-buddy

This is a small helper for transmission and vpns in containerized setups (docker, kubernetes etc.)
It runs alongside transmission and a vpn container and can monitor a file containing a port number.
Everytime the file changes it updates transmissions peer port setting. This useful for VPN provider
where you can get a portforwarding, but have no control over the actual port. Some VPN container write
the forwarded port to a file. With a shared volume transmission-buddy can pickup on these changes.

## Features

* Watches for updated port forwarding information in the specified file
* Updates transmission peer port based on that information
* Simple health check which succeeds if the specified port is open and fails if it's not

## Configuration

transmission-buddy is configured with environment variables:


| Variable | Default |
|----------|---------|
| BUDDY_TRANSMISSION_PORT | 9091 |
| BUDDY_TRANSMISSION_HOST | 127.0.0.1 |
| BUDDY_TRANSMISSION_USERNAME | |
| BUDDY_TRANSMISSION_PASSWORD | |
| BUDDY_LOG_LEVEL             | info |
| BUDDY_PORT_PATH             | /pia-shared/port.dat |
| BUDDY_HEALTH_ADDR           | :8080 |

## Known issues

* When running on kubernetes as a sidecard with transmission and the vpn container in the same pod, the firewall rules of
  the vpn sidecar might prevent the healthcheck from working, causing a timeout. You then need to include the network of your
  k8s nodes in `LOCAL_NETWORK` env variable of your vpn container.

## Planned features

* Simple prometheus metrics based on transmission session metrics
* Simple prometheus metrics based on vpn network metrics
